package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me

        Member getMember = guild.getMember("Nemesis", "Ordinary");
//        List<Member> memberList = guild.getMemberList();

        assertEquals(null, getMember);
        assertEquals(1, guild.getMemberList().size());

        Member newMember = new OrdinaryMember("Nemesis", "Ordinary");
        guild.addMember(guildMaster, newMember);

        getMember = guild.getMember("Nemesis", "Ordinary");
//        memberList = guild.getMemberList();

        assertEquals(newMember, getMember);
        assertEquals(2, guild.getMemberList().size());

    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me

        Member getMember = guild.getMember("Nemesis", "Ordinary");
//        List<Member> memberList = guild.getMemberList();

        assertEquals(null, getMember);
        assertEquals(1, guild.getMemberList().size());

        Member newMember = new OrdinaryMember("Nemesis", "Ordinary");
        guild.addMember(guildMaster, newMember);
        getMember = guild.getMember("Nemesis", "Ordinary");
//        memberList = guild.getMemberList();

        assertEquals(newMember, getMember);
        assertEquals(2, guild.getMemberList().size());

        guild.removeMember(guildMaster, newMember);
        getMember = guild.getMember("Nemesis", "Ordinary");
//        memberList = guild.getMemberList();

        assertEquals(null, getMember);
        assertEquals(1, guild.getMemberList().size());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member getMaster = guild.getMember("Eko", "Master");

        assertEquals(guildMaster, getMaster);

        Member getMember = guild.getMember("Nemesis", "Ordinary");

        assertEquals(null, getMember);

        Member newMember = new OrdinaryMember("Nemesis", "Ordinary");
        guild.addMember(guildMaster, newMember);
        getMember = guild.getMember("Nemesis", "Ordinary");

        assertEquals(newMember, getMember);

        getMember = guild.getMember("Nonexistant", "This That");

        assertEquals(null, getMember);
    }
}
