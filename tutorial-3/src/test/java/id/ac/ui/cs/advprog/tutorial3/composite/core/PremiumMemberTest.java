package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me

        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me

        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me

//        List<Member> childList = member.getChildMembers();

        assertEquals(0, member.getChildMembers().size());

        Member newMember = new OrdinaryMember("New", "Member");
        member.addChildMember(newMember);

        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me

//        List<Member> childList = member.getChildMembers();

        assertEquals(0, member.getChildMembers().size());

        Member newMember = new OrdinaryMember("New", "Member");
        member.addChildMember(newMember);

        assertEquals(1, member.getChildMembers().size());

        member.removeChildMember(newMember);

        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
    }
}
