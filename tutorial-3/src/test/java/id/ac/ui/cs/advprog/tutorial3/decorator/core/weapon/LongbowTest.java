package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LongbowTest {


    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Longbow();
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals("Long Bow", weapon.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        assertEquals("This thing launch arrow", weapon.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        assertEquals(15, weapon.getWeaponValue());
    }
}
