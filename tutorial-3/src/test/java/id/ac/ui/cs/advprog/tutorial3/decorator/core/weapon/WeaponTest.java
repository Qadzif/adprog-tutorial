package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class WeaponTest {

    Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = Mockito.mock(Weapon.class);
    }

    @Test
    public void testMethodGetWeaponName(){
        Mockito.doCallRealMethod().when(weapon).getName();
        String weaponName = weapon.getName();
        //TODO: Complete me
        verify(weapon, times(1)).getName();
    }

    @Test
    public void testMethodGetWeaponDescription(){
        Mockito.doCallRealMethod().when(weapon).getDescription();
        String weaponDescription = weapon.getDescription();
        //TODO: Complete me
        verify(weapon, times(1)).getDescription();
    }

    @Test
    public void testMethodGetWeaponValue(){
        Mockito.doCallRealMethod().when(weapon).getWeaponValue();
        int weaponValue = weapon.getWeaponValue();
        //TODO: Complete me
        verify(weapon, times(1)).getWeaponValue();
    }
}
