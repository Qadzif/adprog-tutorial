package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
    //TODO: Complete me
    
    public Longbow () {
        weaponName = "Long Bow";
        weaponDescription = "This thing launch arrow";
        weaponValue = 15;
    }
}
