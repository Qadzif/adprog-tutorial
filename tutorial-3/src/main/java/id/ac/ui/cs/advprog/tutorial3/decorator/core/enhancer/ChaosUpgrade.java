package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    private static Random rand = new Random();

    public ChaosUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        return weapon.getWeaponValue() + rand.nextInt(6) + 50;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription() + " [Chaos]";
    }
}
