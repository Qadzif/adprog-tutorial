package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Shield extends Weapon {
    //TODO: Complete me

    public Shield () {
        weaponName = "Shield";
        weaponDescription = "This thing can block";
        weaponValue = 10;
    }
}
