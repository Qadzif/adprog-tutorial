package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
    //TODO: Complete me

    public Sword () {
        weaponName = "Sword";
        weaponDescription = "This thing can slash";
        weaponValue = 25;
    }
}
