package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    //TODO: Complete me
    
    String name, role;
    List<Member> childList = new ArrayList<>();

    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
    }
    
    @Override
    public String getName() { return this.name; }
    
    @Override
    public String getRole() { return this.role; }

    @Override
    public void addChildMember(Member member) {
        this.childList.add(member);
    }
    
    @Override
    public void removeChildMember(Member member) {
        this.childList.remove(member);
    }
    
    @Override
    public List<Member> getChildMembers() { return childList; }
}
