package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Gun extends Weapon {
    //TODO: Complete me

    public Gun () {
        weaponName = "Gun";
        weaponDescription = "This thing launch bullet";
        weaponValue = 20;
    }
}
