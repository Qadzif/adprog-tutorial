package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                //ToDo: Complete Me
		this.guild = guild;
		guild.add(this);
        }

        //ToDo: Complete Me
	public void update () {
		if (!guild.getQuestType().equals("E")) {
			getQuests().add(guild.getQuest());
		}
	} 
}
