package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                //ToDo: Complete Me
		this.guild = guild;
		guild.add(this);
        }

        //ToDo: Complete Me
	public void update () {
		if (!guild.getQuestType().equals("R")) {
			getQuests().add(guild.getQuest());
		}
	}
}
