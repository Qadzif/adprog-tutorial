package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;	

@Service
public class GuildServiceImpl implements GuildService {
        private final QuestRepository questRepository;
        private final Guild guild;
        private final Adventurer agileAdventurer;
        private final Adventurer knightAdventurer;
        private final Adventurer mysticAdventurer;

        public GuildServiceImpl(QuestRepository questRepository) {
                this.questRepository = questRepository;
                this.guild = new Guild();
                //ToDo: Complete Me
		agileAdventurer = new AgileAdventurer(guild);
		knightAdventurer = new KnightAdventurer(guild);
		mysticAdventurer = new MysticAdventurer(guild);
        }

        //ToDo: Complete Me
	public List<Adventurer> getAdventurers () {
		return guild.getAdventurers();
	}

	public void addQuest (Quest quest) {
		if (quest.getType() != "") {
			guild.addQuest(quest);
			questRepository.save(quest);
		}
	}
}
