package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
        //ToDo: Complete me

    @Override
    public String attack() {
        return "Kazam";
    }

    @Override
    public String getType() {
        return "Magic";
    }
}
